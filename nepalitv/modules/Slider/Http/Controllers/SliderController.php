<?php namespace Modules\Slider\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class SliderController extends Controller {
	
	public function index()
	{
		$data=array('id'=>'slider');
		return view('slider::index',$data);
	}
	
}