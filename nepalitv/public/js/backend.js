
              CKEDITOR.replace( 'description' );
/*Cropper 1*/
/*------------------------------------------------------------------------------------------------------------*/
$(function() {
      var $cropper = $(".cropper"),
          $dataX = $("#dataX"),
          $dataY = $("#dataY"),
          $dataH = $("#dataH"),
          $dataW = $("#dataW"),
          cropper;

      $cropper.cropper({
         aspectRatio: 1920/ 680,
        data: {
          x: 500,
          y: 150,
          width: 1920,
          height: 680
        },
        preview: ".preview",

        // autoCrop: false,
        // dragCrop: false,
        // modal: false,
        // moveable: false,
        // resizeable: false,

        // maxWidth: 480,
        // maxHeight: 270,
        // minWidth: 160,
        // minHeight: 90,

        done: function(data) {
          $dataX.val(data.x);
          $dataY.val(data.y);
          $dataH.val(data.height);
          $dataW.val(data.width);
        },
        build: function(e) {
          console.log(e.type);
        },
        built: function(e) {
          console.log(e.type);
        },
        dragstart: function(e) {
          console.log(e.type);
        },
        dragmove: function(e) {
          console.log(e.type);
        },
        dragend: function(e) {
          console.log(e.type);
        }
      });

      cropper = $cropper.data("cropper");

      $cropper.on({
        "build.cropper": function(e) {
          console.log(e.type);
          // e.preventDefault();
        },
        "built.cropper": function(e) {
          console.log(e.type);
          // e.preventDefault();
        },
        "dragstart.cropper": function(e) {
          console.log(e.type);
          // e.preventDefault();
        },
        "dragmove.cropper": function(e) {
          console.log(e.type);
          // e.preventDefault();
        },
        "dragend.cropper": function(e) {
          console.log(e.type);
          // e.preventDefault();
        }
      });

  


      $("#setAspectRatio").click(function() {
        $cropper.cropper("setAspectRatio", $("#aspectRatio").val());
      });
      $("#uploadImagecrop").change(function(){
          var p = $("#cropper");
    var _URL = window.URL || window.webkitURL;

        p.fadeOut();
        var ext = $('#uploadImagecrop').val().split('.').pop().toLowerCase();
        if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
            // alert('invalid extension!');
            $("#uploadImagecrop").val("");
        }

        // prepare HTML5 FileReader
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("uploadImagecrop").files[0]);

        oFReader.onload = function (oFREvent) {
            // p.attr('src', oFREvent.target.result).fadeIn();
              $cropper.cropper("setImgSrc", oFREvent.target.result);
        };

         });
      $("#getImgInfo").click(function() {
        $("#showInfo").val(JSON.stringify($cropper.cropper("getImgInfo")));
      });

      $("#getData").click(function() {
        $("#showData").val(JSON.stringify($cropper.cropper("getData")));
      });
    });
/*---------------------------------------------------------------------------------------------------*/
var p = $("#cropper");
    var _URL = window.URL || window.webkitURL;

    // prepare instant preview
    $("#uploadImage").change(function(e){
        // fadeOut or hide preview
        var file, img,aw;
        if ((file = this.files[0])) {
            img = new Image();
            img.onload = function () {
                // alert(this.width + " " + this.height);
                aw= (this.width)/700;
                $('#chag_sort').val(aw);
            };
            img.src = _URL.createObjectURL(file);
        }
        p.fadeOut();
        var ext = $('#uploadImage').val().split('.').pop().toLowerCase();
        if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
            // alert('invalid extension!');
            $("#uploadImage").val("");
        }

        // prepare HTML5 FileReader
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("uploadImage").files[0]);

        oFReader.onload = function (oFREvent) {
            p.attr('src', oFREvent.target.result).fadeIn();
        };

    });
    /*----------------------------------------------------------------------------------------------------*/
