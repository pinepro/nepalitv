  <!-- Main Featured Slider -->
  <div class="cp-featured-news-slider">
    <div class="featured-slider">
      <div class="item">
        <div class="cp-post-content">
          <div class="catname"><a class="btn btn-lorange waves-effect waves-button" href="category-layout-2.html">Sports</a> </div>
          <h1><a href="single-post.html">Gonna Make a History in World Cup</a></h1>
          <ul class="cp-post-tools">
            <li><i class="icon-1"></i> May 10, 2015</li>
            <li><i class="icon-2"></i> Nelson Doe</li>
            <li><i class="icon-3"></i> Lifestyle</li>
            <li><i class="icon-4"></i> 57 Comments</li>
          </ul>
        </div>
        <div class="cp-post-thumb"><img src="images/fsimg1.jpg" alt=""></div>
      </div>
      <div class="item">
        <div class="cp-post-content">
          <div class="catname"><a class="btn btn-pink waves-effect waves-button" href="category-layout-1.html">Fashion</a> </div>
          <h1><a href="single-post.html">Trends of Fashion in 2015</a></h1>
          <ul class="cp-post-tools">
            <li><i class="icon-1"></i> May 10, 2015</li>
            <li><i class="icon-2"></i> Nelson Doe</li>
            <li><i class="icon-3"></i> Lifestyle</li>
            <li><i class="icon-4"></i> 57 Comments</li>
          </ul>
        </div>
        <div class="cp-post-thumb"><img src="images/fsimg2.jpg" alt=""></div>
      </div>
      <div class="item">
        <div class="cp-post-content">
          <div class="catname"><a class="btn btn-purple waves-effect waves-button" href="category-layout-4.html">LifeStyle</a> </div>
          <h1><a href="single-post.html">An Emotions Behind the Smile...</a></h1>
          <ul class="cp-post-tools">
            <li><i class="icon-1"></i> May 10, 2015</li>
            <li><i class="icon-2"></i> Nelson Doe</li>
            <li><i class="icon-3"></i> Lifestyle</li>
            <li><i class="icon-4"></i> 57 Comments</li>
          </ul>
        </div>
        <div class="cp-post-thumb"><img src="images/fsimg3.jpg" alt=""></div>
      </div>
      <div class="item">
        <div class="cp-post-content">
          <div class="catname"><a class="btn btn-orange waves-effect waves-button" href="category-layout-3.html">Photography</a> </div>
          <h1><a href="single-post.html">Attractive Photography in openly</a></h1>
          <ul class="cp-post-tools">
            <li><i class="icon-1"></i> May 10, 2015</li>
            <li><i class="icon-2"></i> Nelson Doe</li>
            <li><i class="icon-3"></i> Lifestyle</li>
            <li><i class="icon-4"></i> 57 Comments</li>
          </ul>
        </div>
        <div class="cp-post-thumb"><img src="images/fsimg4.jpg" alt=""></div>
      </div>
    </div>
  </div>
  <!-- Main Featured End --> 