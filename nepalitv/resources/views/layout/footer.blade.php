  
  <!-- Footer Start -->
  <footer id="footer" class="footer">
    <div class="footer-one footer-widgets">
      <div class="container">
        <div class="row">
          <div class="col-md-3"> 
            
            <!-- Text Widget -->
            <div class="widget textwidget">
              <h3>Text Widget</h3>
              <div class="cp-widget-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                <p> Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat.</p>
              </div>
            </div>
            <!-- Text Widget End --> 
          </div>
          <div class="col-md-3"> 
            <!-- Fatured posts -->
            <div class="widget">
              <h3>Featured Posts</h3>
              <div class="cp-widget-content">
                <ul class="featured-posts">
                  <li>
                    <h4><a href="#">Morbi iaculis eros eget urna...</a></h4>
                    <ul class="cp-post-tools">
                      <li><i class="icon-1"></i> May 10, 2015</li>
                      <li><i class="icon-4"></i> 57 Comments</li>
                    </ul>
                  </li>
                  <li>
                    <h4><a href="#">Morbi iaculis eros eget urna...</a></h4>
                    <ul class="cp-post-tools">
                      <li><i class="icon-1"></i> May 09, 2015</li>
                      <li><i class="icon-4"></i> 15 Comments</li>
                    </ul>
                  </li>
                  <li>
                    <h4><a href="#">Morbi iaculis eros eget urna...</a></h4>
                    <ul class="cp-post-tools">
                      <li><i class="icon-1"></i> May 11, 2015</li>
                      <li><i class="icon-4"></i> 60 Comments</li>
                    </ul>
                  </li>
                  <li>
                    <h4><a href="#">Morbi iaculis eros eget urna...</a></h4>
                    <ul class="cp-post-tools">
                      <li><i class="icon-1"></i> May 15, 2015</li>
                      <li><i class="icon-4"></i> 80 Comments</li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
            <!-- Fatured posts End --> 
          </div>
          <div class="col-md-3"> 
            <!-- Popular posts -->
            <div class="widget">
              <h3>Latest Reviews</h3>
              <div class="cp-widget-content">
                <ul class="reviews">
                  <li>
                    <h4><a href="#">Fusce quis nisi cursus</a></h4>
                    <div class="cp-rating"><a href="#"><i class="icon-9"></i> <i class="icon-9"></i> <i class="icon-9"></i> <i class="icon-9"></i> <i class="icon-10"></i></a></div>
                    <i class="tag">8.2</i> </li>
                  <li>
                    <h4><a href="#">Morbi vel metus vitae</a></h4>
                    <div class="cp-rating"><a href="#"><i class="icon-9"></i> <i class="icon-9"></i> <i class="icon-9"></i> <i class="icon-9"></i> <i class="icon-10"></i></a></div>
                    <i class="tag">9.2</i> </li>
                  <li>
                    <h4><a href="#">Proin ut sapien tempor</a></h4>
                    <div class="cp-rating"><a href="#"><i class="icon-9"></i> <i class="icon-9"></i> <i class="icon-9"></i> <i class="icon-9"></i> <i class="icon-10"></i></a></div>
                    <i class="tag">7.2</i> </li>
                  <li>
                    <h4><a href="#">Vivamus feugiat lacus</a></h4>
                    <div class="cp-rating"><a href="#"><i class="icon-9"></i> <i class="icon-9"></i> <i class="icon-9"></i> <i class="icon-9"></i> <i class="icon-10"></i></a></div>
                    <i class="tag">5.2</i> </li>
                </ul>
              </div>
            </div>
            <!-- Popular posts End --> 
          </div>
          <div class="col-md-3"> 
            <!-- Popular Posts -->
            <div class="widget popular-posts ">
              <h3>Popular</h3>
              <div class="cp-widget-content">
                <ul class="small-grid">
                  <li>
                    <div class="small-post">
                      <div class="cp-thumb"><img src="images/pgthumb-6.jpg" alt=""></div>
                      <div class="cp-post-content">
                        <h4><a href="#">Quisque sit amet est</a></h4>
                        <ul class="cp-post-tools">
                          <li><i class="icon-1"></i> May 10, 2015</li>
                          <li><i class="icon-4"></i> 57 Comments</li>
                        </ul>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="small-post">
                      <div class="cp-thumb"><img src="images/pgthumb-1.jpg" alt=""></div>
                      <div class="cp-post-content">
                        <h4><a href="#">Quisque sit amet est</a></h4>
                        <ul class="cp-post-tools">
                          <li><i class="icon-1"></i> May 10, 2015</li>
                          <li><i class="icon-4"></i> 57 Comments</li>
                        </ul>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="small-post">
                      <div class="cp-thumb"><img src="images/pgthumb-2.jpg" alt=""></div>
                      <div class="cp-post-content">
                        <h4><a href="#">Quisque sit amet est</a></h4>
                        <ul class="cp-post-tools">
                          <li><i class="icon-1"></i> May 10, 2015</li>
                          <li><i class="icon-4"></i> 57 Comments</li>
                        </ul>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
            
            <!-- Popular Posts End --> 
            
          </div>
        </div>
      </div>
    </div>
    <div class="footer-two footer-widgets">
      <div class="container">
        <div class="row">
          <div class="col-md-3">
            <div class="widget form-widget">
              <h3>Stay connected</h3>
              <div class="cp-widget-content">
                <form  class="material">
                  <div class="input-group">
                    <input type="text" name="name" placeholder="Name" required>
                    <input type="email" name="email" placeholder="Email Address" required>
                    <input type="text" name="subject" placeholder="Subject">
                    <textarea name="message" placeholder="Message"></textarea>
                  </div>
                  <button class="btn btn-submit waves-effect waves-button" type="submit">Submit <i class="fa fa-angle-right"></i></button>
                </form>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="widget twitter-widget">
              <h3>Latest Tweets</h3>
              <div class="cp-widget-content">
                <ul class="tweets">
                  <li><a href="#">@nelson.doe</a>
                    <div class="tweets_txt">Nunc nisl tortor, pretium quis magna a, egestas laoreet lectus <span>themeforest.net/item/materialmag</span></div>
                  </li>
                  <li><a href="#">@nelson.doe</a>
                    <div class="tweets_txt">Proin ipsum elit, varius eu tempor sit amet, rutrum a justo neque eget <span>themeforest.net/item/materialmag</span></div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="widget gallery-widget">
              <h3>Gallery</h3>
              <div class="cp-widget-content">
                <div id="sync1" class="owl-carousel">
                  <div class="item"><img src="images/fg-1.jpg" alt=""></div>
                  <div class="item"><img src="images/fg-1.jpg" alt=""></div>
                  <div class="item"><img src="images/fg-1.jpg" alt=""></div>
                  <div class="item"><img src="images/fg-1.jpg" alt=""></div>
                  <div class="item"><img src="images/fg-1.jpg" alt=""></div>
                </div>
                <div id="sync2" class="owl-carousel">
                  <div class="item"><img src="images/fg-2.jpg" alt=""></div>
                  <div class="item"><img src="images/fg-3.jpg" alt=""></div>
                  <div class="item"><img src="images/fg-4.jpg" alt=""></div>
                  <div class="item"><img src="images/fg-2.jpg" alt=""></div>
                  <div class="item"><img src="images/fg-4.jpg" alt=""></div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="widget contact-widget">
              <h3>Contact Info</h3>
              <div class="cp-widget-content">
                <address>
                <ul>
                  <li> <i class="fa fa-university"></i>
                    <p>The Material Magazine Building,
                      123 South Road, Industrial Avenue, 1234 New York, U.S.A.</p>
                  </li>
                  <li> <i class="fa fa-phone"></i>
                    <p> Phone: 0123 456 7890<br>
                      Fax: 0800 080 1234 </p>
                  </li>
                  <li> <i class="fa fa-skype"></i>
                    <p> Skype: +12 8564 232 </p>
                  </li>
                  <li> <i class="fa fa-envelope-o"></i>
                    <p> Email: <a href="mailto:info@materialmag.com">info@materialmag.com</a> </p>
                  </li>
                  <li> <i class="fa fa-globe"></i>
                    <p> <a href="#">www.materialmag.com</a> </p>
                  </li>
                </ul>
                </address>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-three">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <div class="footer-logo"><img src="images/footer-logo.png" alt=""></div>
          </div>
          <div class="col-md-6">
            <ul class="footer-social">
              <li> <a href="#"><i class="fa fa-twitter"></i></a> </li>
              <li> <a href="#"><i class="fa fa-facebook"></i></a> </li>
              <li> <a href="#"><i class="fa fa-pinterest"></i></a> </li>
              <li> <a href="#"><i class="fa fa-linkedin"></i></a> </li>
              <li> <a href="#"><i class="fa fa-google-plus"></i></a> </li>
              <li> <a href="#"><i class="fa fa-youtube"></i></a> </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-four">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <p>All Rights Reserved 2015 © MaterialMag, Designed & Developed by <a href="http://crunchPress.com" target="_blank">CrunchPress.com</a></p>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!-- Footer End --> 