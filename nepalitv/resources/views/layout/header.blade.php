 <!-- Header Start -->
  <div id="cp-header" class="cp-header"> 
    
    <!-- Topbar Start -->
    <div class="cp-topbar">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <ul class="toplinks">
              <li class="waves-effect waves-button"><a href="index.html">Home</a></li>
              <li class="waves-effect waves-button"><a href="#">News</a></li>
              <li class="waves-effect waves-button"><a href="#">FAQ’s</a></li>
              <li class="waves-effect waves-button"><i class="fa fa-phone"></i> + 800 123 4567</li>
              <li class="waves-effect waves-button"><i class="fa fa-envelope-o"></i> <a href="mailto:info@materialmag.com">info@materialmag.com</a></li>
            </ul>
          </div>
          <div class="col-md-6">
            <div class="cp-toptools pull-right">
              <ul>
                <li class="waves-effect"><a href="login.html"><i class="icon-2"></i></a></li>
                <li class="waves-effect"><a href="register.html"><i class="fa fa-sign-in"></i></a></li>
                <li class="waves-effect"><a href="#"><i class="fa fa-cart-arrow-down"></i></a></li>
              </ul>
            </div>
            <div class="cp-topsocial pull-right">
              <ul>
                <li class="waves-effect"><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li class="waves-effect"><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li class="waves-effect"><a href="#"><i class="fa fa-linkedin"></i></a></li>
                <li class="waves-effect"><a href="#"><i class="fa fa-youtube"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Topbar End --> 
    
    <!-- Logo row Start -->
    <div class="cp-logo-row">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <div class="logo"><a href="index.html"><img src="images/logo.png" alt=""></a></div>
          </div>
          <div class="col-md-8">
            <div class="cp-advertisement waves-effect"><img src="images/ad-large.gif" alt=""></div>
          </div>
        </div>
      </div>
    </div>
    <!-- Logo row Start --> 
    
    <!-- Mega Menu Start -->
    <div class="cp-megamenu">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="cp-mega-menu">
              <label for="mobile-button"> <i class="fa fa-bars"></i> </label>
              <!-- mobile click button to show menu -->
              <input id="mobile-button" type="checkbox">
              <ul class="collapse main-menu">
              <li class="slogo"><a href="index.html"><img src="images/logo-micon.png" alt=""></a></li>
                
                <li><a href="index.html">Home</a>
                  <ul class="drop-down one-column hover-expand">
                    <!-- first level drop down -->
                    <li> <a href="index.html">Home Layout One</a> </li>
                    <li> <a href="index-layout-02.html">Home Layout Two</a> </li>
                    <li> <a href="index-layout-03.html">Home Layout Three</a> </li>
                  </ul>
                </li>
                <li> <a href="#">Fashion</a>
                  <ul class="drop-down full-width col-5 hover-expand">
                    <!-- full width drop down with 5 columns + images -->
                    <li class="validation">
                      <h2 class="mm-title">Fashion</h2>
                    </li>
                    <li><img src="images/mm-1.jpg" alt="">
                      <h3><a href="category-layout-1.html">Proin id diam in nulla sagit tempor nec eu ipsum.</a></h3>
                    </li>
                    <li><img src="images/mm-2.jpg" alt="">
                      <h3><a href="category-layout-1.html">Sed pulvinar quam non ultricies lacinia.</a></h3>
                    </li>
                    <li><img src="images/mm-3.jpg" alt="">
                      <h3><a href="category-layout-1.html">Nullam tincidunt lorem sit amet imperdiet sollicit.</a></h3>
                    </li>
                    <li><img src="images/mm-4.jpg" alt="">
                      <h3><a href="category-layout-1.html">Sed pulvinar quam non ultricies lacinia.</a></h3>
                    </li>
                    <li><img src="images/mm-5.jpg" alt="">
                      <h3><a href="category-layout-1.html">Nullam tincidunt lorem sit amet imperdiet sollicitu.</a></h3>
                    </li>
                  </ul>
                </li>
                <li> <a href="#">Lifestyle</a>
                  <ul class="drop-down full-width col-5 hover-expand">
                    <!-- full width drop down with 5 columns + images -->
                    <li class="validation">
                      <h2 class="mm-title">Lifestyle</h2>
                    </li>
                    <li><img src="images/mm-1.jpg" alt="">
                      <h3><a href="category-layout-4.html">Proin id diam in nulla sagit tempor nec eu ipsum.</a></h3>
                    </li>
                    <li><img src="images/mm-2.jpg" alt="">
                      <h3><a href="category-layout-4.html">Sed pulvinar quam non ultricies lacinia.</a></h3>
                    </li>
                    <li><img src="images/mm-3.jpg" alt="">
                      <h3><a href="category-layout-4.html">Nullam tincidunt lorem sit amet imperdiet sollicit.</a></h3>
                    </li>
                    <li><img src="images/mm-4.jpg" alt="">
                      <h3><a href="#">Sed pulvinar quam non ultricies lacinia.</a></h3>
                    </li>
                    <li><img src="images/mm-5.jpg" alt="">
                      <h3><a href="category-layout-4.html">Nullam tincidunt lorem sit amet imperdiet sollicitu.</a></h3>
                    </li>
                  </ul>
                </li>
                <li> <a href="#">Sports</a>
                  <ul class="drop-down full-width col-4 hover-expand">
                    <!-- full width drop down with 5 columns + images -->
                    <li>
                      <ul class="sub-menu">
                        <li> <a href="#">About</a> </li>
                        <li> <a href="#">Testimonials</a> </li>
                        <li> <a href="author-archives.html">Archives</a> </li>
                        <li> <a href="gallery-full.html">Gallery</a> </li>
                        <li> <a href="contact.html">Contact Page</a> </li>
                        <li> <a href="page-404.html">404 Page</a> </li>
                      </ul>
                    </li>
                    <li><img src="images/mm-2.jpg" alt="">
                      <h3><a href="category-layout-2.html">Sed pulvinar quam non ultricies lacinia.</a></h3>
                    </li>
                    <li><img src="images/mm-3.jpg" alt="">
                      <h3><a href="category-layout-2.html">Nullam tincidunt lorem sit amet imperdiet sollicit.</a></h3>
                    </li>
                    <li><img src="images/mm-4.jpg" alt="">
                      <h3><a href="category-layout-2.html">Sed pulvinar quam non ultricies lacinia.</a></h3>
                    </li>
                  </ul>
                </li>
                <li> <a href="#">Photography</a>
                  <ul class="drop-down full-width col-5 hover-expand">
                    <!-- full width drop down with 5 columns + images -->
                    <li class="validation">
                      <h2 class="mm-title">Photography</h2>
                    </li>
                    <li><img src="images/mm-1.jpg" alt="">
                      <h3><a href="category-layout-3.html">Proin id diam in nulla sagit tempor nec eu ipsum.</a></h3>
                    </li>
                    <li><img src="images/mm-2.jpg" alt="">
                      <h3><a href="category-layout-3.html">Sed pulvinar quam non ultricies lacinia.</a></h3>
                    </li>
                    <li><img src="images/mm-3.jpg" alt="">
                      <h3><a href="category-layout-3.html">Nullam tincidunt lorem sit amet imperdiet sollicit.</a></h3>
                    </li>
                    <li><img src="images/mm-4.jpg" alt="">
                      <h3><a href="category-layout-3.html">Sed pulvinar quam non ultricies lacinia.</a></h3>
                    </li>
                    <li><img src="images/mm-5.jpg" alt="">
                      <h3><a href="category-layout-3.html">Nullam tincidunt lorem sit amet imperdiet sollicitu.</a></h3>
                    </li>
                  </ul>
                </li>
                <li> <a href="#">Pages</a>
                  <ul class="drop-down one-column hover-fade">
                    <!-- first level drop down -->
                    
                    <li> <a href="author-archives-sidebar.html">Post Format Archive</a> </li>
                    <li> <a href="#">Single Post</a> </li>
                    <li> <a href="#">WooCommerce Shop</a><i class="fa fa-angle-right"></i>
                      <ul class="drop-down one-column hover-expand">
                        <!-- second level drop down -->
                        <li> <a href="products.html">Shop</a> </li>
                        <li> <a href="product-details.html">Product Details</a> </li>
                        <li> <a href="login.html">Login</a> </li>
                        <li> <a href="register.html">Registers</a> </li>
                      </ul>
                    </li>
                    <li> <a href="authors.html">Author</a> </li>
                    <li> <a href="author-archives.html">Author Archive</a> </li>
                    <li> <a href="date-archives.html">Date Archive</a> </li>
                    <li> <a href="tags.html">Tag Archive</a> </li>
                    <li> <a href="search-results.html">Search Results</a> </li>
                    <li> <a href="contact.html">Contact Page</a> </li>
                    <li> <a href="page-404.html">404 Page</a> </li>
                    <li> <a href="#">Gallery</a> <i class="fa fa-angle-right"></i> <!-- fontAwesome icon -->
                      
                      <ul class="drop-down one-column hover-expand">
                        <!-- second level drop down -->
                        <li> <a href="gallery-classic.html">Gallery Classic</a> </li>
                        <li> <a href="gallery-elite.html">Gallery Elite View</a> </li>
                        <li> <a href="gallery-full.html">Gallery Full</a> </li>
                        <li> <a href="gallery-2col.html">Gallery Large</a> </li>
                        <li> <a href="gallery-masonry.html">Gallery Masonry</a> </li>
                        <li> <a href="gallery-medium.html">Gallery Medium</a> </li>
                        <li> <a href="gallery-small.html">Gallery Small</a> </li>
                      </ul>
                    </li>
                  </ul>
                </li>
                <li> <a href="#">Blog</a>
                  <ul class="drop-down full-width blog-menu hover-expand">
                    <li>
                      <ul>
                        <li> <a href="#"> <img src="images/mm-1.jpg" alt=""> </a>
                          <h3><a href="#">Proin id diam in nulla sagittempor</a></h3>
                        </li>
                        <li> <a href="#"> <img src="images/mm-2.jpg" alt=""> </a>
                          <h3><a href="#">Proin id diam in nulla sagittempor</a></h3>
                        </li>
                      </ul>
                    </li>
                    <li>
                      <ul>
                        <li> <a href="#"> <img src="images/mm-3.jpg" alt=""> </a>
                          <h3><a href="#">Proin id diam in nulla sagittempor</a></h3>
                        </li>
                        <li> <a href="#"> <img src="images/mm-4.jpg" alt=""> </a>
                          <h3><a href="#">Proin id diam in nulla sagittempor</a></h3>
                        </li>
                      </ul>
                    </li>
                    <li>
                      <ul>
                        <!-- column three -->
                        <li class="validation">
                          <h2>Blog Layouts</h2>
                        </li>
                        <li> <a href="blog-full.html"> Blog Full</a> </li>
                        <li> <a href="blog-medium.html"> Blog Medium</a> </li>
                        <li> <a href="blog-column.html"> Blog Colum</a> </li>
                        <li> <a href="blog-grid-modern.html"> Blog Grid Modren</a> </li>
                        <li> <a href="blog-top-featured.html"> Blog Top Featured</a> </li>
                        <li> <a href="blog-masonry.html"> Blog Masonry</a> </li>
                        <li> <a href="single-post.html"> Single Post</a> </li>
                      </ul>
                    </li>
                    <li>
                      <ul>
                        <!-- column four -->
                        <li class="validation">
                          <h2>Blog Category</h2>
                        </li>
                        <li> <a href="category-layout-3.html">Photography</a> </li>
                        <li> <a href="category-layout-2.html">Sports</a> </li>
                        <li> <a href="category-layout-1.html">Fashion</a> </li>
                        <li> <a href="category-layout-4.html">Lifestyle</a> </li>
                        <li> <a href="#">World</a> </li>
                        <li> <a href="#">Health</a> </li>
                        <li> <a href="#">Technology</a> </li>
                      </ul>
                    </li>
                  </ul>
                </li>
                <li><a href="author-archives-sidebar.html">Archive</a></li>
                <li> <a href="#">Shortcodes</a>
                  <div class="drop-down full-width text-links hover-expand"> <!-- full width drop down with 4 columns -->
                    <ul>
                      <!-- column one -->
                      <li> <a href="#">About</a> </li>
                      <li> <a href="#">Testimonials</a> </li>
                      <li> <a href="#">Archives</a> </li>
                      <li> <a href="#">Gallery</a> </li>
                      <li> <a href="#">Contact Page</a> </li>
                      <li> <a href="#">404 Page</a> </li>
                    </ul>
                    <ul>
                      <!-- column one -->
                      <li> <a href="#">About</a> </li>
                      <li> <a href="#">Testimonials</a> </li>
                      <li> <a href="#">Archives</a> </li>
                      <li> <a href="#">Gallery</a> </li>
                      <li> <a href="#">Contact Page</a> </li>
                      <li> <a href="#">404 Page</a> </li>
                    </ul>
                    <ul>
                      <!-- column one -->
                      <li> <a href="#">About</a> </li>
                      <li> <a href="#">Testimonials</a> </li>
                      <li> <a href="#">Archives</a> </li>
                      <li> <a href="#">Gallery</a> </li>
                      <li> <a href="#">Contact Page</a> </li>
                      <li> <a href="#">404 Page</a> </li>
                    </ul>
                    <ul>
                      <!-- column one -->
                      <li> <a href="#">About</a> </li>
                      <li> <a href="#">Testimonials</a> </li>
                      <li> <a href="#">Archives</a> </li>
                      <li> <a href="#">Gallery</a> </li>
                      <li> <a href="#">Contact Page</a> </li>
                      <li> <a href="#">404 Page</a> </li>
                    </ul>
                  </div>
                </li>
                <li><a href="contact.html">Contact Us</a></li>
                <li class="search-bar"> <i class="icon-7"></i> <!-- search bar -->
                  
                  <ul class="drop-down hover-expand">
                    <li>
                      <form method="post" >
                        <table>
                          <tr>
                            <td><input type="text" name="serach_bar" placeholder="Type Keyword Here"></td>
                            <td><input type="submit" value="Search"></td>
                          </tr>
                        </table>
                      </form>
                    </li>
                  </ul>
                </li>
                <li class="random"><a href="random.html"><i class="icon-6"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Mega Menu End --> 
    
  </div>
  <!-- Header End --> 
