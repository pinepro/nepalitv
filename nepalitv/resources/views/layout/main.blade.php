
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Nepali Tv">
<link rel="shortcut icon" href="images/fav.ico" type="image/x-icon" />
<link href="public/css/custom.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="public/css/color.css" rel="stylesheet">
<!-- color CSS -->
<link href="public/css/mega-menu.css" rel="stylesheet">
<!-- Mega Menu -->
<link href="public/css/bootstrap.css" rel="stylesheet">
<!-- Bootstrap -->
<link href="public/css/bootstrap-theme.min.css" rel="stylesheet">
<!-- Bootstrap -->
<link href="public/css/materialize.css" rel="stylesheet">
<!-- Materialize CSS -->
<link href="public/css/font-awesome.min.css" rel="stylesheet">
<!-- Font Awesome CSS -->
<link href="public/css/owl.slider.css" rel="stylesheet">

			@yield("css")
</head>
<body>
<!-- 
<div id="{{ $id }}"> -->

	<!-- Main Wrapper Start -->
	<div id="wrapper" class="wrapper"> 

		@include("layout.header")

		@include("layout.slider")

			@yield("content")

		<!-- Footer -->
		@include("layout.footer")

	</div>
<!-- </div> -->
	<!-- scripts -->
	
<!-- Js Files--> 
<script src="public/js/jquery-1.11.2.min.js"></script> 
<script src="public/js/jquery-migrate-1.2.1.min.js"></script> 
<script src="public/js/bootstrap.min.js"></script> 
<script src="public/js/materialize.min.js"></script> 
<script src="public/js/owl.carousel.min.js"></script>
<script src="public/js/custom.js"></script>

		@yield("scripts")
</body>
</html>
