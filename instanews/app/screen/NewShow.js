/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */

import React, {
  AppRegistry,
  Component,
  StyleSheet,
  Text,
  Image,
  ListView,
  TouchableOpacity,
  BackAndroid,
  WebView ,
  View
} from 'react-native';
import ViewContainer from '../components/ViewContainer'
import StatusBarBackground from '../components/StatusBarBackground'
import Icon from 'react-native-vector-icons/FontAwesome'
var HEADER = '#3b5998'; var BGWASH = 'rgba(255,255,255,0.8)'; var DISABLED_WASH = 'rgba(255,255,255,0.25)'; var TEXT_INPUT_REF = 'urlInput'; var WEBVIEW_REF = 'webview'; var DEFAULT_URL = 'https://m.facebook.com';
class NewShow extends Component {
  constructor(props){
    super(props)
    this.state={
      status: 'No Page Loaded',
  backButtonEnabled:true , 
      forwardButtonEnabled: true, 
      loading: true, 
      scalesPageToFit: true
    }
    BackAndroid.addEventListener('hardwareBackPress', () => {if (this.props.navigator) {this.props.navigator.pop(); return true;} return false;});
 
  }
  render() {

    return (

        <ViewContainer>
        <Text style={styles.title}>
          {this.props.news.title}
        </Text>
           <WebView ref={WEBVIEW_REF}
            automaticallyAdjustContentInsets={false}
             style={styles.webView} 
             source={{uri: this.props.news.url}} 
             javaScriptEnabled={true} 
             domStorageEnabled={true} 
             decelerationRate="normal"
              onNavigationStateChange={this.onNavigationStateChange}
               onShouldStartLoadWithRequest={this.onShouldStartLoadWithRequest} 
               startInLoadingState={true}
                scalesPageToFit={this.state.scalesPageToFit} />
        </ViewContainer>
        
    );

  }

}
const styles = StyleSheet.create({

  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
   webView: { backgroundColor: 'skyblue', height: 350, },
  title: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  }

});



module.exports= NewShow
