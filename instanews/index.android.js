/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */

import React, {
  AppRegistry,

  StyleSheet,
  Component,
  Navigator,
  Text,
  View
} from 'react-native';
import NewsList from './app/screen/NewsList'

import NewShow from './app/screen/NewShow'

class instanews extends Component {
  _renderScene(route,navigator){
    var globalNavigatorProps={
      navigator
    }
    switch(route.ident){
        case "NewsList":
        return (
          <NewsList
            {...globalNavigatorProps}
          />
        )
        case "NewShow":
         return (
          <NewShow
            {...globalNavigatorProps}
            news={route.news}
          />
        )
        default:
          return(
          <Text>
            Something Went Wrong!!
          </Text>
        )

    }
  }
  configureScene(){
    return Navigator.SceneConfigs.FloatFromBottom;
  }
   render() {

    return(
      <Navigator
      configureScene={this.configureScene}
       initialRoute= {{ident:"NewsList"}}
       ref="appNavigator"
       style={styles.navigatorStyle}
        renderScene={(route, navigator) => {return this._renderScene(route,navigator)}}
       />

      )
  }
  
}

const styles = StyleSheet.create({
  navigatorStyle:{

  }

});

AppRegistry.registerComponent('instanews', () => instanews);
